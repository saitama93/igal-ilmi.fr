<!DOCTYPE html>
<!--[if IE 8 ]><html class="no-js oldie ie8" lang="fr"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="fr"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="no-js" lang="fr">
<!--<![endif]-->

<head>

    <!--- basic page needs
   ================================================== -->
    <meta charset="utf-8">
    <title>Portfolio Développeur Web de ILMI AMIR Igal</title>
    <meta name="description" content="Portfolio Développeur Web de ILMI AMIR Igal">
    <meta name="author" content="ILMI AMIR Igal">
    <meta name="keywords" content="Développeur web, PHP, Symfony"/>
    <meta name="robots" content="index"/>
    <meta name="robots" content="follow"/>
    
    <!-- Open Graph meta pour Facebook -->
    <meta property="og:title" content="Portfolio Igal" />
    <meta property="og:url" content="http://www.igal-ilmi.fr/" />
    <meta property="og:image" content="http://www.igal-ilmi.fr/images/portfolio/preview.jpg" />
    <meta property="og:description" content="Portfolio Développeur Web de ILMI AMIR Igal" />
    <meta property="og:site_name" content="ILMI AMIR Igal" />
    <meta property="og:type" content="website" />

    <!-- Card meta pour Twitter -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@igalilmi32">
    <meta name="twitter:title" content="Portfolio Igal" />
    <meta name="twitter:description" content="Portfolio Développeur Web de ILMI AMIR Igal" />
    <!-- Twitter summary card avec image large de 280x150px -->
    <meta name="twitter:image:src" content="/images/portfolio/preview.jpg" />

    <!-- mobile specific metas
   ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
   ================================================== -->
    <link rel="stylesheet" href="css/base.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/vendor.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- script
   ================================================== -->
    <script src="js/modernizr.js"></script>
    <script src="js/pace.min.js"></script>

    <!-- favicons
	================================================== -->
    <link rel="icon" type="image/png" href="favicon.png">

</head>

<body id="top">

    <!-- header 
   ================================================== -->
    <header>
        <div class="row">

            <div class="top-bar">
                <a class="menu-toggle" href="#"><span>Menu</span></a>

                <div class="logo">
                    <a href="index.html">IAI</a>
                </div>

                <nav id="main-nav-wrap">
                    <ul class="main-navigation">
                        <li class="current"><a class="smoothscroll" href="#intro" title="">Accueil</a></li>
                        <li><a class="smoothscroll" href="#about" title="">A propos</a></li>
                        <li><a class="smoothscroll" href="#resume" title="">Expériences</a></li>
                        <li><a class="smoothscroll" href="#portfolio" title="">Portfolio</a></li>
                        <li><a class="smoothscroll" href="#contact" title="">Contact</a></li>
                    </ul>
                </nav>
            </div>
            <!-- /top-bar -->

        </div>
        <!-- /row -->
    </header>
    <!-- /header -->

    <!-- intro section
   ================================================== -->
    <section id="intro">

        <div class="intro-overlay"></div>

        <div class="intro-content">
            <div class="row">

                <div class="col-twelve">

                    <h5>Hello, World.</h5>
                    <h1>Je m'appelle ILMI AMIR <span class="firstName">Igal</span></h1>

                    <p class="intro-position">
                        <span>Développeur d' Application</span>
                        <span>PHP / Angular</span>
                    </p>

                    <a class="button stroke smoothscroll" href="#about" title="">A propos de moi</a>

                </div>

            </div>
        </div>
        <!-- /intro-content -->

        <ul class="intro-social">
            <li><a href="https://www.linkedin.com/in/igal-ilmi-amir/"><i class="fa fa-brands fa-linkedin"></i></a></li>
            <li><a href="https://twitter.com/igalilmi32"><i class="fa fa-twitter"></i></a></li>
            <li><a href="https://gitlab.com/saitama93"><i class="fa fa-github"></i></a></li>
        </ul>
        <!-- /intro-social -->

    </section>
    <!-- /intro -->


    <!-- about section
   ================================================== -->
    <section id="about">

        <div class="row section-intro">
            <div class="col-twelve">

                <h5>A propos de moi</h5>
                <h1>Laissez-moi me présenter.</h1>

                <div class="intro-info">

                    <img src="images/photo_portfolio.jpeg" alt="Profile Picture">

                    <p class="lead">Je suis un concepteur développeur d'application (PHP / Angular) qui est aussi passionné par la partie ops.</p>
                </div>

            </div>
        </div>
        <!-- /section-intro -->

        <div class="row about-content">

            <div class="col-six tab-full">

                <h3>Profile</h3>
                <p>Passionné de programmation et vivant dans une ère où le numérique est maître. Je suis disponible pour une nouvelle expérience en tant que développeur php/symfony au sein d'une équipe professionnelle.</p>

                <ul class="info-list">
                    <li>
                        <strong>Nom complet:</strong>
                        <span>ILMI AMIR Igal</span>
                    </li>
                    <li>
                        <strong>Âge:</strong>
                        <span>30 ans</span>
                    </li>
                    <li>
                        <strong>Poste actuel:</strong>
                        <span>Dévelloppeur web, Groupe SII</span>
                    </li>
                    <li>
                        <strong>Site internet:</strong><br>
                        <a href="https://sii-group.com/fr-FR" target="_blank">www.sii-group.com/fr-FR</a>
                    </li>
                    <li>
                        <strong>Mail:</strong>
                        <span>igalilmi@hotmail.fr</span>
                    </li>

                </ul>
                <!-- /info-list -->

            </div>

            <div class="col-six tab-full">

                <h3>Compétences</h3>
                <p>Curieux, autonome, altruiste et souriant sont les premières caractéristiques qui me viennent à l'esprit lorsqu'on me demande de me décrire.</p>

                <ul class="skill-bars">
                    <li>
                        <strong>Symfony</strong>
                    </li>
                    <li>
                        <strong>PHP</strong>
                    </li>
                    <li>
                        <strong>Angular</strong>
                    </li>
                    <li>
                        <strong>Javascript / Typescript</strong>
                    </li>
                    <li>
                        <strong>Docker</strong>
                    </li>
                    <li>
                        <strong>Linux</strong>
                    </li>
                </ul>
                <!-- /skill-bars -->

            </div>

        </div>

        <div class="row button-section">
            <div class="col-twelve">
                <a href="#contact" title="Hire Me" class="button stroke smoothscroll">Embauchez-moi</a>
                <a href="/files/CV_igal.pdf" target="_blank" download="CV_igal" title="Download CV" class="button button-primary">Télécharger CV</a>
            </div>
        </div>

    </section>
    <!-- /process-->


    <!-- resume Section
   ================================================== -->
    <section id="resume" class="grey-section">

        <div class="row section-intro">
            <div class="col-twelve">

                <h5>Parcours</h5>
                <h1>Plus sur mon parcours</h1>

                <p class="lead">
                J’ai un parcours atypique, en effet, après avoir renforcé mes connaissances en algorithmie en fac de science, j’ai voulu m’orienter vers le développement web via des formations diplômantes.  
                <p>

            </div>
        </div>
        <!-- /section-intro-->

        <div class="row resume-timeline">

            <div class="col-twelve resume-header">

                <h2>Experiences</h2>

            </div>
            <!-- /resume-header -->

            <div class="col-twelve">

                <div class="timeline-wrap">

                    <div class="timeline-block">

                        <div class="timeline-ico">
                            <i class="fa fa-briefcase"></i>
                        </div>

                        <div class="timeline-header">
                            <h3>Développeur web </h3>
                            <p>Avril 2023 - aujourd'hui</p>
                        </div>

                        <div class="timeline-content">
                            <h4>Groupe SII - CDI</h4>
                            <p>Développements logiciels avec Symfony et Angular. Montée de version(Angular 13 à 15), mise en prod, rédaction de documentation technique</p>
                        </div>

                    </div>
                    <!-- /timeline-block -->

                    <div class="timeline-block">

                        <div class="timeline-ico">
                            <i class="fa fa-briefcase"></i>
                        </div>

                        <div class="timeline-header">
                            <h3>Développeur web </h3>
                            <p>Septembre 2022 - Mars 2023</p>
                        </div>

                        <div class="timeline-content">
                            <h4>6TM - CDI</h4>
                            <p>Développement de nouvelles fonctionnalité, maintenance de code existant, développements d'applications avec Symfony, optimisation des performances</p>
                        </div>

                    </div>
                    <!-- /timeline-block -->

                    <div class="timeline-block">

                        <div class="timeline-ico">
                             <i class="fa fa-briefcase"></i>
                        </div>

                        <div class="timeline-header">
                            <h3>Apprenti Concepteur / Développeur d'application </h3>
                            <p>Octobre 2020 - Septembre 2022</p>
                        </div>

                        <div class="timeline-content">
                            <h4>Département du doubs - Alternance</h4>
                            <p>L'objectif premier de mon alternance est la montée en compétence. Je dois réaliser une application de type gestion des tâches pour l'ensemble du service. Un peu comme confluence de Atlassian</p>
                        </div>

                    </div>
                    <!-- /timeline-block -->

                    <div class="timeline-block">

                        <div class="timeline-ico">
                             <i class="fa fa-briefcase"></i>
                        </div>

                        <div class="timeline-header">
                            <h3>Développeur Web junior</h3>
                            <p>Février 2020 à mars 2020</p>
                        </div>

                        <div class="timeline-content">
                            <h4>CRIF Formation &amp; Conseil - CDD</h4>
                            <p>Réalisation d'un site vitrine multi pages avec Wordpress</p>
                        </div>

                    </div>
                    <!-- /timeline-block -->

                    <div class="timeline-block">

                        <div class="timeline-ico">
                             <i class="fa fa-briefcase"></i>
                        </div>

                        <div class="timeline-header">
                            <h3>Développeur Web junior</h3>
                            <p>Septembre 2019 à novembre 2019</p>
                        </div>

                        <div class="timeline-content">
                            <h4>CRIF Formation &amp; Conseil - Stage</h4>
                            <p>Le CRIF avait comme solution de gestion d'émargement, un papier à faire signer pas leurs stagiaires. L'objectif de mon stage était de trouver une solution pour numériser cette tâche. J'ai pu réaliser une application, avec Symfony
                                4, une solution qui répond à cette problématique.</p>
                        </div>

                    </div>
                    <!-- /timeline-block -->

                </div>
                <!-- /timeline-wrap -->

            </div>
            <!-- /col-twelve -->

        </div>
        <!-- /resume-timeline -->

        <div class="row resume-timeline">

            <div class="col-twelve resume-header">

                <h2>Education</h2>

            </div>
            <!-- /resume-header -->

            <div class="col-twelve">

                <div class="timeline-wrap">

                    <div class="timeline-block">

                        <div class="timeline-ico">
                            <i class="fa fa-graduation-cap"></i>
                        </div>

                        <div class="timeline-header">
                            <h3>Concepteur Développeur d'Application</h3>
                            <p>Octobre 2020 à août 2022</p>
                        </div>

                        <div class="timeline-content">
                            <h4>Ecole ENI Bac +4</h4>
                            <p>Cette formation CDA a pour objectif :</p>
                            <ul>
                                <li> - La prise en charge des études fonctionnelles et techniques</li>
                                <li> - La spécification et conception technique d'une application informatique de type client/serveur n-tiers ou mobile</li>
                                <li> - La programmation d'une application informatique</li>
                                <li> - La participation à la gestion de projet informatique</li>
                            </ul>
                        </div>

                    </div>
                    <!-- /timeline-block -->

                    <div class="timeline-block">

                        <div class="timeline-ico">
                            <i class="fa fa-graduation-cap"></i>
                        </div>

                        <div class="timeline-header">
                            <h3>Développeur web &amp; web mobile junior</h3>
                            <p>Mars 2019 à novembre 2019</p>
                        </div>

                        <div class="timeline-content">
                            <h4>Access code school Bac+2</h4>
                            <p>Cette formation m'a permis d'apprendre à :</p>
                            <ul>
                                <li> - concevoir une application fonctionnelle et performante pour répondre aux besoins du client</li>
                                <li> - établir un product backlog, des wireframes et des maquettes</li>
                                <li> - maîtriser HTML5 et CSS3, qui sont à la base de tous les sites web</li>
                                <li> - utiliser une API, concevoir une base de données, écrire et optimiser les requêtes SQL, utiliser un gestionnaire de versions et bien sûr à écrire du code lisible et maintenable rendre les pages web dynamiques et interactives,
                                    se spécialiser dans un langages de programmation et un framework d'application : PHP/Symfony
                                </li>
                                <li> - faire une veille technologique qui est indispensable tout au long d'une carrière de développeur</li>
                            </ul>
                        </div>

                    </div>
                    <!-- /timeline-block -->

                    <div class="timeline-block">

                        <div class="timeline-ico">
                            <i class="fa fa-graduation-cap"></i>
                        </div>

                        <div class="timeline-header">
                            <h3>Conseillé médiateur numérique</h3>
                            <p>Septembre 2018 à mars 2019</p>
                        </div>

                        <div class="timeline-content">
                            <h4>Digifab Bac+2</h4>
                            <p>Durant cette formation, j'ai pu voir les bases de la programmation avec Arduino et la création de site avec le CMS WordPress.</p>
                        </div>

                    </div>
                    <!-- /timeline-block -->

                    <div class="timeline-block">

                        <div class="timeline-ico">
                            <i class="fa fa-graduation-cap"></i>
                        </div>

                        <div class="timeline-header">
                            <h3>Licence informatique</h3>
                            <p>Septembre 2012 à juin 2015</p>
                        </div>

                        <div class="timeline-content">
                            <h4>UFR-ST Besançon</h4>
                        </div>

                    </div>
                    <!-- /timeline-block -->

                    <div class="timeline-block">

                        <div class="timeline-ico">
                            <i class="fa fa-graduation-cap"></i>
                        </div>

                        <div class="timeline-header">
                            <h3>BAC général série scientifique</h3>
                            <p>Septembre 2011 à juin 2012</p>
                        </div>

                        <div class="timeline-content">
                            <h4>Lycée d'état de Djibouti</h4>
                        </div>

                    </div>
                    <!-- /timeline-block -->

                </div>
                <!-- /timeline-wrap -->

            </div>
            <!-- /col-twelve -->

        </div>
        <!-- /resume-timeline -->

    </section>
    <!-- /features -->


    <!-- Portfolio Section
   ================================================== -->
    <section id="portfolio">

        <div class="row section-intro">
            <div class="col-twelve">

                <h5>Portfolio</h5>
                <h1>Retrouvez certains de mes projets.</h1>
            </div>
        </div>
        <!-- /section-intro-->

        <div class="row portfolio-content">

            <div class="col-twelve">

                <!-- portfolio-wrapper -->
                <div id="folio-wrapper" class="block-1-2 block-mob-full stack">
                    <!-- folio-item -->
                    <div class="bgrid folio-item">
                        <div class="item-wrap">
                            <img src="images/portfolio/symbnb.png" alt="Symbnb">
                            <a href="#modal-01" class="overlay">
                                <div class="folio-item-table">
                                    <div class="folio-item-cell">
                                        <h3 class="folio-title">Symbnb</h3>
                                        <span class="folio-types">
		     					       	  Site marchand
		     					       </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- /folio-item -->
                   
                    <!-- folio-item -->
                    <div class="bgrid folio-item">
                        <div class="item-wrap">
                            <img src="images/portfolio/env_docker-symfony.jpg" alt="Shutterbug">
                            <a href="#modal-02" class="overlay">
                                <div class="folio-item-table">
                                    <div class="folio-item-cell">
                                        <h3 class="folio-title">Env Symfony/Docker</h3>
                                        <span class="folio-types">
		     					       	  Dev
		     					      </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    
                    <!-- /folio-item -->

                    <div class="bgrid folio-item">
                        <div class="item-wrap">
                            <img src="images/portfolio/dashboard.png" alt="Clouds">
                            <a href="#modal-03" class="overlay">
                                <div class="folio-item-table">
                                    <div class="folio-item-cell">
                                        <h3 class="folio-title">PANDA</h3>
                                        <span class="folio-types">
                                        Outil collaboratif 
		     					      </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div id="modal-01" class="popup-modal slider mfp-hide">

                        <div class="media">
                            <img src="images/portfolio/symbnb.png" alt="" />
                        </div>

                        <div class="description-box">
                            <h4>Symbnb</h4>
                            <p>SymBNB est un projet personnel que j'ai réalisé avec un ami réalisé avec le framework PHP Symfony (la version 4). Ce site est une copie d'Airbnb qui reprend à peu près toutes les fonctionnalités du site Airbnb.</p>

                            <div class="categories">DÉVELOPPEMENT WEB</div>
                        </div>

                        <div class="link-box">
                            <a href="https://symbandb.herokuapp.com/" target="_blank">Voir site</a>
                            <a href="#" class="popup-modal-dismiss">Fermer</a>
                        </div>

                    </div>
                    <!-- /modal-01 -->

                    <div id="modal-02" class="popup-modal slider mfp-hide">

                        <div class="media">
                            <img src="images/portfolio/modals/env_docker-symfony.jpg" alt="" />
                        </div>

                        <div class="description-box">
                            <h4>Env Symfony / Docker</h4>
                            <p>
                            Ce dépôt GitLab est une "doc" pour la mise en place rapide d'un environnement de dev sous docker pour symfony. Il me permet à moi et tous ceux qui le veulent à rapidement mettre en place un environnement de dev pour leur projets informatique.
                            </p>

                            <div class="categories">Documentation</div>
                        </div>

                        <div class="link-box">
                            <a href="https://gitlab.com/saitama93/image-docker-avec-symfony-5-php7.4-composer-et-apache" target="_blank">Voir dépôt</a>
                            <a href="#" class="popup-modal-dismiss">Fermer</a>
                        </div>

                    </div>
                    <!-- /modal-02 -->

                    <div id="modal-03" class="popup-modal slider mfp-hide">

                        <div class="media">
                            <img src="images/portfolio/dashboard.png" alt="" />
                        </div>

                        <div class="description-box">
                            <h4>
                                PANDA (
                                    <span class="special-char">P</span>ilotage & 
                                    <span class="special-char">A</span>nimation
                                    <span class="special-char">N</span>umérique
                                    <span class="special-char">D</span>es
                                    <span class="special-char">A</span>ctivité)
                            </h4>
                            <p>
                                Panda est une app un peu comme <a href="https://www.atlassian.com/software/confluence" class="confluence">Confluence</a> de Atlassian, c'est-à-dire qu'on pourra, en se connectant au préalable, avoir des détails sur un projet et le droit d'apporter des modifications à celui-ci en fonction des droits de l'utilisateur connecté. De telle façon à ce que le directeur, les chefs de service ou les chefs de projet ai une vue personnalisée sur ses détails.
                            </p>

                            <div class="categories">DÉVELOPPEMENT WEB</div>
                        </div>

                        <div class="link-box">
                            <a href="#" onclick="return false;" class="isDisabled">Voir site</a>
                            <a href="#" class="popup-modal-dismiss">Fermer</a>
                        </div>

                    </div>
                    <!-- /modal-03 -->

                    <div id="modal-04" class="popup-modal slider mfp-hide">

                        <div class="media">
                            <img src="images/portfolio/modals/m-beetle.jpg" alt="" />
                        </div>

                        <div class="description-box">
                            <h4>Beetle</h4>
                            <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>

                            <div class="categories">DÉVELOPPEMENT WEB</div>
                        </div>

                        <div class="link-box">
                            <a href="http://www.behance.net">Details</a>
                            <a href="#" class="popup-modal-dismiss">Close</a>
                        </div>

                    </div>
                    <!-- /modal-04 -->

                    <div id="modal-05" class="popup-modal slider mfp-hide">

                        <div class="media">
                            <img src="images/portfolio/modals/m-lighthouse.jpg" alt="" />
                        </div>

                        <div class="description-box">
                            <h4>Lighthouse</h4>
                            <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>

                            <div class="categories">DÉVELOPPEMENT WEB</div>
                        </div>

                        <div class="link-box">
                            <a href="http://www.behance.net">Details</a>
                            <a href="#" class="popup-modal-dismiss">Close</a>
                        </div>

                    </div>
                    <!-- /modal-05 -->

                    <div id="modal-06" class="popup-modal slider mfp-hide">

                        <div class="media">
                            <img src="images/portfolio/modals/m-salad.jpg" alt="" />
                        </div>

                        <div class="description-box">
                            <h4>Salad</h4>
                            <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>

                            <div class="categories">Branding</div>
                        </div>

                        <div class="link-box">
                            <a href="http://www.behance.net">Details</a>
                            <a href="#" class="popup-modal-dismiss">Close</a>
                        </div>

                    </div>

                </div>
                <!-- /portfolio-wrapper -->

            </div>
            <!-- /twelve -->

        </div>
        <!-- /portfolio-content -->

    </section>
    <!-- /portfolio -->

    <section id="services">

		<div class="overlay"></div>

		<div class="row section-intro">
   		<div class="col-twelve">

   			<h5>Veille</h5>
   			<h1>Compétences en cours d'apprentissage</h1>

   			<p class="lead">
                   Dans le domaine du développement web, l'apprentissage ne s'arrête jamais. Le développeur doit toujours se mettre à la page pour ne pas être dépassé par les nouvelles technologies/méthodologie, c'est pourquoi la veille est un élément important dans ce domaine. Voici une liste de ma veille actuelle.
            </p>

   		</div>   		
   	</div> <!-- /section-intro -->

   	<div class="row services-content">

   		<div id="owl-slider" class="owl-carousel services-list">

	      	<div class="service">	

	      		<span class="icon"><img src="images/icons/docker.png" alt="docker icon"></span>            

	            <div class="service-content">	

	            	 <h3>Docker</h3>

		            <p class="desc">
                        Docker permet de gérer de manière simplifiée la dépendance, mais au fond, y a-t-il un réel intérêt ? Honnêtement, oui !
                        Il nous est tous arrivé de passer plusieurs heures à essayer d’installer et de lancer un projet car beaucoup de facteurs problématiques peuvent entrer en jeu, tels que le système d’exploitation, la base de données, ou encore la version du langage de développement.
	         		</p>
	         		
	         	</div> 	         	 

				</div> <!-- /service -->

				<div class="service">	

					<span class="icon"><img src="images/icons/clean-code.png" alt="docker icon"></i></span>                          

	            <div class="service-content">	

	            	<h3>Clean code / Architecture Hexagonale</h3>

		            <p class="desc">
                        Le but du Clean Code permet d’écrire du code plus propre, plus facile à lire et surtout mieux structuré. Le code que l'on produis doit pouvoir être lu et compris aisément par d’autres développeurs, peu importe leurs niveaux. Ce qui facilitera sa modification et son évolution pour les années à venir.
	         		</p>

	            </div>	                          

			   </div> <!-- /service -->

			   <div class="service">

			   	<span class="icon"><img src="images/icons/tdd.png" alt="docker icon"></span>		            

	            <div class="service-content">

	            	<h3>TDD</h3>

		            <p class="desc">
                        Le TDD (Test Driven Development) est une pratique qui consiste à développer les tests avant d’écrire le code applicatif. De cette manière, les tests ne sont plus écrits en fonction du code, ce qui est le cas dans les processus actuels de développement. On évite ainsi de se retrouver dans la situation où, à cause de la manière dont l’application a été codée, on ne peut tester certaines parties.
                        Grâce au TDD, c’est au code de s’adapter aux tests, en travaillant de manière itérative jusqu’à arriver à un développement finalisé, testé à 100%
	        		</p> 

	            </div> 	            	               

			   </div> <!-- /service -->
	      </div> <!-- /services-list -->
   		
   	</div> <!-- /services-content -->
		
	</section> <!-- /services -->	



    <!-- contact
   ================================================== -->
    <section id="contact">

        <div class="row section-intro">
            <div class="col-twelve">

                <h5>Contact</h5>
                <h1>Prennons un rendez-vous.</h1>
            </div>
        </div>
        <!-- /section-intro -->

        <div class="row contact-form">

            <div class="col-twelve">

                <!-- form -->
                <form name="contactForm" id="contactForm" method="post" action="">
                    <fieldset>

                        <div class="form-field">
                            <input name="contactName" type="text" id="contactName" placeholder="Nom" value="" minlength="2" required="">
                        </div>
                        <div class="form-field">
                            <input name="contactEmail" type="email" id="contactEmail" placeholder="Mail" value="" required="">
                        </div>
                        <div class="form-field">
                            <input name="contactSubject" type="text" id="contactSubject" placeholder="Objet" value="">
                        </div>
                        <div class="form-field">
                            <textarea name="contactMessage" id="contactMessage" placeholder="Message" rows="10" cols="50" required=""></textarea>
                        </div>
                        <div class="form-field">
                            <button class="submitform">Envoyer</button>
                            <div id="submit-loader">
                                <div class="text-loader">Envoie...</div>
                                <div class="s-loader">
                                    <div class="bounce1"></div>
                                    <div class="bounce2"></div>
                                    <div class="bounce3"></div>
                                </div>
                            </div>
                        </div>

                    </fieldset>
                </form>
                <!-- Form End -->

                <!-- contact-warning -->
                <div id="message-warning">
                </div>
                <!-- contact-success -->
                <div id="message-success">
                    <i class="fa fa-check"></i>Votres message a été envoyé, merci!<br>
                </div>

            </div>
            <!-- /col-twelve -->

        </div>
        <!-- /contact-form -->

        <div class="row contact-info">

            <div class="col-four tab-full">

                <div class="icon">
                    <i class="icon-pin"></i>
                </div>

                <h5> Où me trouver</h5>

                <p>
                    Nantes, France<br> 44000
                </p>

            </div>

            <div class="col-four tab-full collapse">

                <div class="icon">
                    <i class="icon-mail"></i>
                </div>

                <h5>Envoyer-moi un mail à</h5>

                <p>igalilmi@hotmail.fr</p>

            </div>

            <div class="col-four tab-full">

                <div class="icon">
                    <i class="icon-phone"></i>
                </div>

                <h5>Appelez-moi au</h5>

                <p>+33 651164483</p>

            </div>

        </div>
        <!-- /contact-info -->

    </section>
    <!-- /contact -->


    <!-- footer
   ================================================== -->

    <footer>
        <div class="row">

            <div class="col-six tab-full pull-right social">

                <ul class="footer-social">
                    <li><a href="https://www.linkedin.com/in/igal-ilmi-amir/" target="_blank"><i class="fa fa-brands fa-linkedin"></i></a></li>
                    <li><a href="https://twitter.com/igalilmi32" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <!-- <li><a href="https://gitlab.com/saitama93"target="_blank"><i class="fa-brands fa-gitlab"></i></a></li> -->
                    <li><a href="https://github.com/saitama93" target="_blank"><i class="fa fa-github"></i></a></li>
                </ul>
                </ul>

            </div>

            <div class="col-six tab-full">
                <div class="copyright">
                    <span>© Copyright Igal <span id="yearCopyright"></span>.</span>
                    <span>Design fait par <a href="http://www.styleshout.com/" target="_blank">styleshout</a></span>
                </div>
            </div>

            <div id="go-top">
                <a class="smoothscroll" title="Back to Top" href="#top"><i class="fa fa-long-arrow-up"></i></a>
            </div>

        </div>
        <!-- /row -->
    </footer>

    <div id="preloader">
        <div id="loader"></div>
    </div>

    <!-- Java Script
   ================================================== -->
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

</body>

</html>